# Projet-Arduino

Objectif:
 
   -  Transmettre des données d’un capteur branché sur une carte Arduino vers un Smartphone Android via bluetooth

Matériel :

   - Arduino Diecemila avec un ATMega 328
   - Module Bluetooth BlueSmirf Silver
   - Cable USB
